# portal

Choose a server type (vless-reality or grpc-tls), copy corresponding info file `info-example.jsonc` from its directory to your VPS, rename the file to `info.jsonc` and
correct it according to your needs. Then copy and run init0.sh as root and follow instrucionts there.

Short summary:

```
# scp servers/vless-reality/info-example.jsonc root@server:~/info.jsonc
# scp init*.sh root@server:~/
# ssh root@server
# nano info.jsonc
# ./init0.sh
# ./init1.sh
# ...
$ cd servers/vless-reality
$ ./init2.sh
$ podman attach xcon
...
```
