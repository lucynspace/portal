// make separate json files with user info from single info.json file;
// these json files can be shared then by nginx

//> using dep io.circe::circe-core:0.14.6
//> using dep io.circe::circe-generic:0.14.6
//> using dep io.circe::circe-parser:0.14.6
//> using dep com.github.pathikrit::better-files:3.9.2

import scala.io.Source
import scala.util.{Try,Success,Failure,Using}
import io.circe._
import io.circe.parser._
import io.circe.generic.semiauto._
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit.DAYS
import java.security.MessageDigest
//import java.util.Base64
import java.math.BigInteger
import java.io.{BufferedWriter,FileWriter}
import java.io.File as JioFile
import better.files._

case class Payment( comment: String, rate: Double, date: String, balance: Double )
case class User( id: String, payment: Payment )
case class ServerSettings( serviceName: String )
case class Info( serverSettings: ServerSettings, users: List[User] )
implicit val paymentDecoder: Decoder[Payment] = deriveDecoder[Payment]
implicit val userDecoder: Decoder[User] = deriveDecoder[User]
implicit val serverSettingsDecoder: Decoder[ServerSettings] = deriveDecoder[ServerSettings]
implicit val infoDecoder: Decoder[Info] = deriveDecoder[Info]

def readFileAsString(filename: String): Try[String] = {
  Try(Source.fromFile(filename).mkString)
}

// rewrite file content
def writeFile(filename: String, content: String): Try[Unit] =
    Using(new BufferedWriter(new FileWriter(new JioFile(filename)))) { bufferedWriter =>
        bufferedWriter.write(content)
    }

// first 12 digits of sha256(s)
def hash(s: String): String = {
  val digest = MessageDigest.getInstance("SHA-256")
  val sha = digest.digest(s.getBytes("UTF-8"))
  //Base64.getEncoder.encodeToString(sha) take 12
  String.format("%032x", new BigInteger( 1, sha ) ) take 12
}

readFileAsString("info.json") match {

  case Success(content) =>
    decode[Info](content) match {
      case Left(parsingError) =>
        println(parsingError)
      case Right(info) =>
        val serviceName = info.serverSettings.serviceName
        val dirname = hash(serviceName)
        File(s"$dirname").createIfNotExists(true)
        info.users.map { user =>
          val fooDate = LocalDate.of(2999, 12, 31)
          val dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
          val startDate =
            Try {
              LocalDate.parse(user.payment.date, dateFormatter)
            } match {
              case Success(s) => s
              case Failure(f) =>
                println(f)
                fooDate
            }
          val rate = user.payment.rate
          val startBalance = user.payment.balance
          val daysInMonth = 31
          val dateUntil = if ( rate < 0.01 ) fooDate else startDate.plusDays( ( startBalance * daysInMonth / rate ).round )
          val now = LocalDate.now
          val balance =
            if ( rate < 0.01 && now.isBefore(startDate) )
              startBalance
            else
              startBalance - rate * DAYS.between( startDate, now ).toDouble / daysInMonth

          /*
          val userInfo = f"""
            |{
            |  "comment": "${user.payment.comment}",
            |  "rate": $rate%.2f,
            |  "balance": $balance%.2f,
            |  "until": "${dateUntil.toString}"
            |}""".stripMargin
          val filename = dirname + "/" + hash(user.id) + ".json"
          */
          val userInfo =
            f"""${user.payment.comment},
               |стоимость доступа $rate%.2f usd в месяц,
               |текущий остаток $balance%.2f usd,
               |слудующая оплата (день-месяц-год) ${dateUntil.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")).toString}
               |""".stripMargin
          val filename = dirname + "/" + hash(user.id) + ".txt"

          File(s"$filename").createIfNotExists()
          writeFile(filename, userInfo) match {
            case Success(_) => {}
            case Failure(f) => println(f)
          }
        }
    }

  case Failure(f) => println(f)

}
